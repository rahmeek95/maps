import * as React from 'react';
// import { View, Text } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from '../srceens/home-screen';
import EventDetailScreen from '../srceens/even-detail-screen';
import { useNavigation } from '@react-navigation/native';
import { navOptions } from './options';
import ProfileDetailScreen from '../srceens/profiles/profiles-detail-screen';
import ProfilesScreen from '../srceens/profiles/profiles-screen';
import { HomeTabs } from './tabs';
import NewEventScreen from '../srceens/new-event-screen';
import Maps from '../srceens/maps/Maps'
// import Login from '../srceens/auth/authentificaton';

const Stack = createNativeStackNavigator();

// export const AuthStack = () => {
//   return (
//     <Stack.Navigator>
//       <Stack.Screen name="Login" component={Login} />
//     </Stack.Navigator>
//   );
// };

export const HomeStack = () =>  {
    const navigation = useNavigation()
    return (

        <Stack.Navigator screenOptions={()=>navOptions(navigation)}>
          <Stack.Screen name="home"  
          component={HomeTabs} />
          <Stack.Screen name="event" component={EventDetailScreen} />
          <Stack.Screen name="new event" component={NewEventScreen} />
        </Stack.Navigator>
    
    )
}    

export const ProfileStack = () =>  {
    const navigation = useNavigation()
    return (

        <Stack.Navigator screenOptions={()=>navOptions(navigation)}>
          <Stack.Screen name="profiles" component={ProfilesScreen} />
          <Stack.Screen name="profile" component={ProfileDetailScreen} />
        </Stack.Navigator>
    
    )
}    
export const MapsStack = () =>  {
  const navigation = useNavigation()
  return (

      <Stack.Navigator screenOptions={()=>navOptions(navigation)}>
        <Stack.Screen name="maps" component={Maps} />
        
      </Stack.Navigator>
  
  )
}    