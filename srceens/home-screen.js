import { useEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet, View, Text, Button } from "react-native";
import EventList from "../components/events/event-list";
import { useGetPostsQuery } from "../redux/services/posts";


const HomeScreen = () => {
    // const navigation = useNavigation();
    const { data, isFetching, isError, refetch } = useGetPostsQuery();

    console.log({useGetPostsQuery});
    // const [data, setData] = useState([]);
    const [refresh, setRefresh] = useState(false)

    const navigation = useNavigation()
    const handleRefresh = () => {
        console.log('refreshing');
        setRefresh(preveState => !preveState)
    }
    useEffect(() => {
        refetch();
      }, []);
    
      if (isFetching) {
        return <Text>En telechargement ...</Text>;
      }
    
      if (isError) {
        return <Text>Erreur pour la recuperation des données </Text>;
      }
    
    // useEffect (()=>{
    //    fetchData()
    // }, [refresh])

    // const fetchData = async () => {
    //     try {
    //         const response = await fetch('http://192.168.1.16:8000/api/events/');
    //         if (!response.ok) {
    //             throw new Error('Réponse HTTP non valide');
    //         }
    //         const data = await response.json();
    //         setData(data);
    //         console.log(data);
    //     } catch (error) {
    //         console.error('Erreur lors de la récupération des données :', error);
    //     }
    // };
    
    return(
        <View style={styles.screen}>
          <Button 
          onPress={()=> navigation.navigate("new event")}
        title="add new event"
          />
          <EventList data={data} onRefresh={handleRefresh} />
        </View>
    )
   
}
const styles = StyleSheet.create({
    screen: {
        padding: 20
    }
})

export default HomeScreen;