import React, { useState, useEffect } from 'react';
import MapView, { Marker } from 'react-native-maps';
import { StyleSheet, View } from 'react-native';
import { requestForegroundPermissionsAsync, getCurrentPositionAsync } from 'expo-location';

let locationsOfInterest = [
  {
    title: "first",
    location: {
      latitude: -27.2,
      longitude: 149
    },
    description: "Mon premier marcher"
  },
  {
    title: "second",
    location: {
      latitude: -38.2,
      longitude: 158
    },
    description: "Mon deuxieme  marcher"
  }
];

export default function App() {
  const [currentLocation, setCurrentLocation] = useState(null);

  useEffect(() => {
    getLocationAsync();
  }, []);

  const getLocationAsync = async () => {
    try {
      const { status } = await requestForegroundPermissionsAsync();
      if (status === 'granted') {
        const location = await getCurrentPositionAsync({});
        setCurrentLocation(location.coords);
      } else {
        console.log('Permission de localisation refusée');
      }
    } catch (error) {
      console.error('Erreur lors de la récupération de la localisation :', error);
    }
  };

  const onRegionChange = (region) => {
    console.log(region);
  };

  const showLocationsOfInterest = () => {
    return locationsOfInterest.map((item, index) => {
      return (
        <Marker 
          key={index}
          coordinate={item.location} 
          title={item.title}
          description={item.description}
        />
      );
    });
  };
 
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        onRegionChange={onRegionChange} 
        initialRegion={{ 
          latitude: 14.704178413690022, 
          longitude: -17.451159421624112,
          latitudeDelta: 0.12393998597200984, 
          longitudeDelta: 0.12286245090029979 
        }}
      >
        {currentLocation && (
          <Marker
            coordinate={currentLocation}
            title="Vous êtes ici"
            pinColor="blue"
          />
        )}
        {showLocationsOfInterest()}
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
});
