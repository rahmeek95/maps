import React, { useState, useLayoutEffect } from 'react';
import { TextInput, View, StyleSheet, Button, Text } from 'react-native';
import { HeaderBackButton } from '@react-navigation/elements';
import { useNavigation } from '@react-navigation/native';
import { useCreateEventMutation } from '../redux/services/posts'; // Vérifiez le chemin vers votre service Redux Toolkit Query

const NewEventScreen = () => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [date, setData] = useState(new Date());
  const [alert, setAlert] = useState({
    isVisible: false,
    msg: '',
  });
  
  const navigation = useNavigation();
  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: 'Ajouter un evenement',
      headerLeft: () => (
        <HeaderBackButton
          tintColor="white"
          onPress={() => navigation.goBack()}
        />
      ),
    });
  });

  const [createEvent, { isLoading, isError }] = useCreateEventMutation();

  const handleAddEvent = () => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('date', date.toISOString().slice(0, 10));

    createEvent(formData)
      .unwrap()
      .then((result) => {
        setAlert({ isVisible: true, msg: 'Evenement ajouter avec succes ' });
      })
      .catch((error) => {
        setAlert({ isVisible: true, msg: 'Erreur de lors de l ajout de l evenement  ' });
        // Gérer les erreurs ici
      });
  };

  return (
    <View style={styles.screen}>
      {alert.isVisible && <Text>{alert.msg}</Text>}
      <TextInput
        value={name}
        onChangeText={setName}
        placeholder="Name"
        style={styles.input}
      />
      <TextInput
        value={description}
        onChangeText={setDescription}
        placeholder="Description"
        style={styles.input}
      />
      <TextInput
        value={date.toISOString().slice(0, 10)}
        onChangeText={(text) => setData(new Date(text))}
        placeholder="Date"
        style={styles.input}
      />
      <Button onPress={handleAddEvent} title="Ajouter un evenement" />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 20,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    backgroundColor: 'white',
    marginBottom: 10,
  },
});

export default NewEventScreen;
 