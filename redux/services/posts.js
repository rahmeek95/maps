import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const postsApi = createApi({
  reducerPath: 'postsApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://192.168.1.16:8000/api' }),
  endpoints: (builder) => ({
    getPosts: builder.query({
      query: () => '/events/',
    }),
    // Ajouter d'autres endpoints
    createEvent: builder.mutation({
      query: (newEvent) => ({
        url: '/events/',
        method: 'POST',
        body: newEvent,
      }),
    }),
  }),
});

export const { useGetPostsQuery, useCreateEventMutation } = postsApi;
