import { configureStore } from "@reduxjs/toolkit";
import { postsApi } from "../services/posts";
import { setupListeners } from "@reduxjs/toolkit/query";




const store = configureStore ({
    reducer: {
        [postsApi.reducerPath]: postsApi.reducer,
        //mes autres recuder a creer 
    },
    middleware: (getDefaultMiddleware) => 
     getDefaultMiddleware().concat(postsApi.middleware),
})

//activer les ecouteurs pour les requettes 

setupListeners(store.dispatch);

export default store;