 import { StyleSheet, Text, View, Button } from 'react-native'
 import React from 'react'
import { useNavigation } from '@react-navigation/native'
 
 const ProfilesScreen = () => {
    const navigation = useNavigation()
   return (
     <View>
       <Text>profiles Screen</Text>
       <Button title="some profile"  onPress={()=> navigation.navigate('Profile', {profileId: 1})} />
     </View>
   )
 }
 
 export default ProfilesScreen
 
 const styles = StyleSheet.create({})