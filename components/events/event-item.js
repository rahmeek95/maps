import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { TouchableOpacity, StyleSheet, Text, Image, View } from 'react-native'

const EventItem = ({id, name, description, qrCode, date}) => {
    const navigation = useNavigation()
  return (
  
        <TouchableOpacity style={styles.card} onPress={()=>navigation.navigate("event", {eventId: id, name, date, description, qrCode})}>
            <View>
              <Text>Nom :{name}</Text>
              <Text>description : {description} </Text>
              <Text>{date} </Text>
            </View>
           
            <Image 
            
              source={{uri: qrCode}}
              style={{ width: 100, height: 100 }}
            />
        </TouchableOpacity>
 
  )
};

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderWidth: 1,
        borderColor: '#5c5c5',
        borderRadius: 10,
        marginVertical: 5,
        padding: 40
    }
})

export default EventItem
