import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../srceens/home-screen';
import EventDetailScreen from '../srceens/even-detail-screen';
import { HomeStack,  MapsStack,  ProfileStack } from './stack';

const Drawer = createDrawerNavigator();

export const MyDrawer = () => {
  return (
    <Drawer.Navigator screenOptions={{headerShown: false}}>
      <Drawer.Screen name="HomeStack" component={HomeStack} 
        options={{
            title: 'HOME'
        }}
      />
       <Drawer.Screen name="ProfilesStack" component={ProfileStack} 
        options={{
            title: 'Profiles'
        }}
      />
      <Drawer.Screen name="maps" component={MapsStack} 
        options={{
            title: 'Maps'
        }}
      />
      {/* <Drawer.Screen name="event" component={EventDetailScreen} /> */}
    </Drawer.Navigator>
  );
}