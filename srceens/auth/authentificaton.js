import { Text, StyleSheet, View, Button } from 'react-native';
import React from 'react';
import { useAuth } from '../context/AuthContext';

export default Login = () => {
  const { signIn } = useAuth();

  const handleSignIn = () => {
    // Appeler la fonction signIn du contexte d'authentification
    signIn();
  };

  return (
    <View style={styles.container}>
      <Text>Page d'authentification</Text>
      <Button title="Se connecter" onPress={handleSignIn} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
