import React, { createContext, useContext, useState } from 'react';

const AuthContext = createContext();

export const useAuth = () => {
  return useContext(AuthContext);
};

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);

  const signIn = () => {
    // Ici, vous implémenteriez la logique de connexion.
    // Par exemple, vous pourriez effectuer une requête à votre backend pour vérifier les informations d'identification.

    // Une fois l'authentification réussie, mettez à jour l'état de l'utilisateur.
    setUser({ username: 'utilisateurTest' });
  };

  const signOut = () => {
    // Ici, vous implémenteriez la logique de déconnexion.
    setUser(null); // Déconnexion en réinitialisant l'état de l'utilisateur à null.
  };

  return (
    <AuthContext.Provider value={{ user, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};
