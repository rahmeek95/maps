import React from 'react'
import { FlatList, RefreshControl, Text, View } from 'react-native'

import EventItem from './event-item'
import { useNavigation } from '@react-navigation/native'

const EventList = ({data, onRefresh}) => {

    const renderItem = ({item}) => {
        return <EventItem  id={item.id} name={item.name} description={item.description} qrCode={item.qr_code} />
    }
   
  return (
    <View>
       <FlatList 
       data={data} 
        keyExtractor={item=> item.id}
        renderItem={renderItem}
        refreshControl={
            <RefreshControl 
            refreshing ={false}
            onRefresh={onRefresh}
            />
        }
       />
    </View>
  )
}

export default EventList